from django.apps import AppConfig


class CicdAppConfig(AppConfig):
    name = 'cicd_app'
