import os
from fabric import Connection as connection, task

host_address = os.environ.get('HOST_ADDRESS')
host_user = os.environ.get('UAT_USER')
host_user_group = os.environ.get('UAT_USER_GROUP')
env_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'env_sample')

@task
def deploy(ctx):
    with connection(host=host_address, user=host_user, inline_ssh_env=True) as conn:
        _set_env_vars(conn)
        _get_latest_source(conn)
        _django_commands(conn)
        _restart_services(conn)

def _get_env_list():
    with open(env_dir, 'r') as f:
        result = []
        for line in f.readlines():
            m = line.strip().split('\n')[0]
            n = m + '=' + str(os.environ.get(m))
            result.append(n)
    return result

def _set_env_vars(conn):
    conn.run('rm ~/cicd_project/.env')
    for i in _get_env_list():
        conn.run('echo %s >> ~/cicd_project/.env' % i)

def _get_latest_source(conn):
    if conn.run('test -d {}'.format('.git'), warn=True).failed:
        conn.run('mkdir {}'.format('.git'))
        conn.run('git init')
        conn.run('git remote add origin git@bitbucket.org:starlord24/fabricdjangotest.git')
        conn.run('git pull origin master')
    else:
        # conn.run('git fetch && git reset --hard origin/master')
        conn.run('git pull origin master')

def _django_commands(conn):
    conn.run(f'venv/bin/pip install -r requirements.txt')
    conn.run(f'venv/bin/python cicd_project/manage.py makemigrations')
    conn.run(f'venv/bin/python cicd_project/manage.py migrate')
    conn.run(f'venv/bin/python cicd_project/manage.py collectstatic --no-input')

def _restart_services(conn):
    conn.run(f'sudo systemctl restart supervisor')
    conn.run(f'sudo service nginx restart')
