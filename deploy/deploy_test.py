# from fabric.api import *
import os

from fabric.connection import Connection

host_address = os.environ.get('HOST_ADDRESS')
host_user = os.environ.get('UAT_USER')

server_user = Connection(host=host_address, user=host_user, inline_ssh_env=True)

server_user.config.run.env = {
                              "SECRET_KEY": os.environ.get('SECRET_KEY'),
                              "DJANGO_SETTINGS_MODULE": os.environ.get('DJANGO_SETTINGS_MODULE'),
                              "DJANGO_ALLOWED_HOSTS": os.environ.get('DJANGO_ALLOWED_HOSTS'),
                              "SQL_ENGINE": os.environ.get('SQL_ENGINE'),
                              "SQL_DATABASE": os.environ.get('SQL_DATABASE'),
                              "SQL_USER": os.environ.get('SQL_USER'),
                              "SQL_PASSWORD": os.environ.get('SQL_PASSWORD'),
                              "SQL_HOST": os.environ.get('SQL_HOST'),
                              "SQL_PORT": os.environ.get('SQL_PORT')
                             }

# server_user.config.run.env = {"DJANGO_SETTINGS_MODULE": os.environ.get('DJANGO_SETTINGS_MODULE')}

# server_user.run('git init')
# server_user.run('git remote add origin git@bitbucket.org:itversity-team/django-cicd-validation.git')
# server_user.run('git clone git@bitbucket.org:itversity-team/django-cicd-validation.git')


server_user.run('git pull origin master')

# Installing requirements
server_user.run('venv/bin/pip install -r requirements.txt')

# Django related operations
server_user.run(f'venv/bin/python cicd_project/manage.py makemigrations')
server_user.run(f'venv/bin/python cicd_project/manage.py migrate')
server_user.run(f'venv/bin/python cicd_project/manage.py collectstatic --no-input')


# localrepo (desktop) - remote_repo (github runner) - remote_host (.git)

